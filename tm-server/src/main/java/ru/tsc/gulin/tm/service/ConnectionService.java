package ru.tsc.gulin.tm.service;

import lombok.SneakyThrows;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.IDatabaseProperty;

import java.sql.Connection;

public class ConnectionService implements IConnectionService {

    @NotNull
    final IDatabaseProperty databaseProperty;

    @NotNull
    private final BasicDataSource dataSource;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.dataSource = dataSource();
    }

    @NotNull
    @SneakyThrows
    public BasicDataSource dataSource() {
        final BasicDataSource result = new BasicDataSource();
        @NotNull final String username = databaseProperty.getDatabaseUser();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseUrl();
        result.setUrl(url);
        result.setUsername(username);
        result.setPassword(password);
        result.setMinIdle(5);
        result.setMaxIdle(10);
        result.setAutoCommitOnReturn(false);
        result.setMaxOpenPreparedStatements(10);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

}
